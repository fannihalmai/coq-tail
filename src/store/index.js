import Vue from "vue";
import axios from "axios";
import Vuex from "vuex";
// import { serviceUrlFilter } from "../../configs";
const resourceUrl = "http://localhost:7000/users";

Vue.use(Vuex);

export const store = new Vuex.Store({
  strict: true,
  state: {
    visibility: false,
    dialogVisibility: false,
    cocktail: {},
    recipes: [],
    user: {},
    userLogged: false,
    displayFavorites: false,
  },
  mutations: {
    setDisplayOnlyFavorites(state, payload) {
      state.displayFavorites = payload.bool;
    },
    setCocktailToDisplay(state, payload) {
      state.cocktail.name = payload.strDrink;
      state.cocktail.id = payload.idDrink;
      state.cocktail.thumb = payload.strDrinkThumb;
      state.cocktail.instruction = payload.strInstructions;
      state.cocktail.ingredients = [
        {
          ingredient: payload.strIngredient1,
          quantity: payload.strMeasure1,
        },
        {
          ingredient: payload.strIngredient2,
          quantity: payload.strMeasure2,
        },
      ];
      if (payload.strIngredient3 != null) {
        state.cocktail.ingredients.push({
          ingredient: payload.strIngredient3,
          quantity: payload.strMeasure3,
        });
      }
      state.recipes.push(state.cocktail);
    },
    setDialogVisible(state, payload) {
      state.dialogVisibility = payload.bool;
    },
    setExistingCocktailToDisplay(state, payload) {
      const coctailToDisplay = state.recipes.find(
        (drink) => (drink.id = payload.id)
      );
      state.cocktail = coctailToDisplay;
    },
    setUserLogged(state, payload) {
      state.userLogged = payload.bool;
    },
    setUser(state, payload) {
      state.user = payload;
    },
    setUserFavoriteDrinks(state, payload) {
      state.user.drinks = payload;
    },
  },
  getters: {
    getUser(state) {
      return state.user;
    },
    getRecipe(state, payload) {
      // TODO if not saved already
      //const drink = state.recipes.find((drink) => (drink.id = payload.id));
      //console.log(drink);
      return state.recipes.find((drink) => (drink.id = payload.id));
    },
    getDialogVisibility(state) {
      return state.dialogVisibility;
    },
    getCocktailToDisplay(state) {
      return state.cocktail;
    },
    favoriteDrinks(state) {
      return state.user.drinks;
    },
  },
  actions: {
    async login({ commit }, payload) {
      const data = {
        email: payload.email,
        password: payload.password,
      };
      const options = {
        method: "POST",
        url: resourceUrl + "/login",
        data: data,
      };
      axios
        .request(options)
        .then(function(response) {
          if (response.data.success) {
            const user = response.data.user;
            commit("setUser", user);
            commit("setUserLogged", { bool: true });
          } else {
            alert("Error occured. Please try again.");
          }
        })
        .catch(function(error) {
          console.error(error);
          alert("Error occured.");
        });
    },
    async signup({ commit }, payload) {
      console.log(payload);
      const data = {
        email: payload.email,
        password: payload.password,
        profile: {
          name: payload.name,
          location: payload.location,
        },
      };
      const options = {
        method: "POST",
        url: resourceUrl,
        data: data,
      };
      console.log(options);
      axios
        .request(options)
        .then(function(response) {
          if (response.status == 200) {
            console.log(commit);
            alert("You successfully created an account. You may now sign in.");
          }
        })
        .catch(function(error) {
          console.log(error);
          alert("Error occured. Please use a valid email address.");
        });
    },
    async addFavoriteDrink({ commit, state }, payload) {
      let array = [];
      if (state.user.drinks.find((el) => el.id === payload.id) === undefined) {
        array = [
          {
            name: payload.name,
            id: payload.id,
            thumb: payload.thumb,
          },
          ...state.user.drinks,
        ];
      } else {
        console.log("taking off from favs");
        state.user.drinks.forEach((el) => {
          if (el.id != payload.id) {
            array.push(el);
          }
        });
      }

      const data = {
        drinks: array,
      };
      const options = {
        method: "PUT",
        url: resourceUrl + "/drinks/" + state.user.id,
        data: data,
      };
      axios
        .request(options)
        .then(function(response) {
          if (response.status == 200) {
            commit("setUserFavoriteDrinks", response.data.drinks);
          }
        })
        .catch(function(error) {
          console.log(error);
          alert("Error occured.");
        });
    },
  },
});
