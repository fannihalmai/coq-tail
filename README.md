# Project Coq-tail

The coq-tail project enables you to browse cocktails based on ingredients of your liking. You can add them to your favorites so that you can find them easily next time when it's apero time 🍹

## Installation

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```
